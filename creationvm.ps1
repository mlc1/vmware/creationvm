############################################## You need to import the right module to connect to vSphere ######################################################
Import-Module VMware.VimAutomation.Core

# Location CSV file
$fileName = '/var/lib/jenkins/workspace/Powershell_Create_VM/vmtotest.csv'

$importData = Import-Csv -path $fileName -delimiter ';'

# IP adress of vSphere
$ipServer = '192.168.255.20'
$password = 'P@ssword123#'

# Report
$TodayTime = Get-Date -UFormat %d-%m-%Y
#$ReportHtml = 'C:\Users\Adnane\Desktop\Automatisation_virtu\Automatisation_virtu\tttoote_rept.html'
$ReportHtml = '/home/jenkins/log' + $TodayTime + '_report.html'

# Start of the report to generate
echo "<FONT color="blue"><h1>Report on $TodayTime </h1></FONT><br /><br />" >> $ReportHtml


Foreach ( $server in $importData ) {
# Date in report
$dateNow = Get-Date -Format "[yyyy/MM/dd HH:mm:ss]"
	# report creation
	$confirmation = Read-Host "
	Name = $($server.Name)
	CPU = $($server.NumCpu) 
	RAM = $($server.MemoryMB) Mo
	disk size = $($server.DiskMB) Mo
	IP server = $($server.VMhost)
	disk type = $($server.DiskStorageFormat)
	OS type = $($server.GuestID)
	Datastore = $($server.Datastore)
	Network Name = $($server.NetworkName)
	Description = $($server.Description)
	Do you want to create $($server.Name) with the following characteristics? (yes/no): 
	Virtual machine $($server.Name) was created `n `n"

	if (( $confirmation -ieq 'y' ) -or ( $confirmation -ieq 'yes' )) { # -ieq accepts uppercase and lowercase
		Write-Host "$($server.Name) will be created: `n" -ForegroundColor Yellow
		if($? -eq "true"){
			add-content -path $ReportHtml -value "$($dateNow) Request to create $($server.Name) accepted <br />"
		}
		# connection to vSphere
		Connect-VIServer -Server $ipServer -Password $password #for more security, there is no  login and password in clear
		if($? -eq "true"){
			add-content -path $ReportHtml -value "$($dateNow) Connection to vSphere succeeded <br />"
		}
		else{
			add-content -path $ReportHtml -value "$($dateNow) Connection to vSphere failed <br />"
		}
		# vm creation command
		New-VM -Name $($server.Name) -VMHost $($server.VMhost) -Datastore $($server.Datastore) -DiskMB $($server.DiskMB) -DiskStorageFormat $($server.DiskStorageFormat) -MemoryMB ($server.MemoryMB) -NumCpu $($server.NumCpu) -GuestId $($server.GuestID) -NetworkName $($server.NetworkName) -Description $($server.Description)
		if($? -eq "true"){
			add-content -path $ReportHtml -value "$($dateNow) $($server.Name) created <br />"
			Write-Host "It's ok, an email will be sent to you !! `n" -ForegroundColor Green
		}
		else{
			add-content -path $ReportHtml -value "$($dateNow) $($server.Name) not created <br />"
			Write-Host "Problem, an email will be sent to you !! `n" -ForegroundColor Red
		}
	}
	elseif (( $confirmation -ieq 'n' ) -or ( $confirmation -ieq 'no' )) {
		if($? -eq "true"){
			add-content -path $ReportHtml -value "$($dateNow) Request to create $($server.Name) deny <br />"
			Write-Host "Request to create $($server.Name) deny `n" -ForegroundColor Red
		}
	}
	else {
		Do {
		$newConfirmation = Read-Host "Answer yes or no"
		}Until (( $newConfirmation -ieq 'y' ) -or ( $newConfirmation -ieq 'yes' ) -or ( $newConfirmation -ieq 'n' ) -or ( $newConfirmation -ieq 'no' )) 
			if (( $newConfirmation -ieq 'y' ) -or ( $newConfirmation -ieq 'yes' )) { # -ieq accepts uppercase and lowercase
				Write-Host "$($server.Name) will be created: `n" -ForegroundColor Yellow
				if($? -eq "true"){
					add-content -path $ReportHtml -value "$($dateNow) Request to create $($server.Name) accepted <br />"
				}
				# connection to vSphere
				Connect-VIServer -Server $ipServer #for more security, there is no password in clear
				if($? -eq "true"){
					add-content -path $ReportHtml -value "$($dateNow) Connection to vSphere succeeded <br />"
				}
				else{
					add-content -path $ReportHtml -value "$($dateNow) Connection to vSphere failed <br />"
				}
				# vm creation command
				New-VM -Name $($server.Name) -VMHost $($server.VMhost) -Datastore $($server.Datastore) -DiskMB $($server.DiskMB) -DiskStorageFormat $($server.DiskStorageFormat) -MemoryMB ($server.MemoryMB) -NumCpu $($server.NumCpu) -GuestId $($server.GuestID) -NetworkName $($server.NetworkName) -Description $($server.Description)
				if($? -eq "true"){
					add-content -path $ReportHtml -value "$($dateNow) $($server.Name) created <br />"
					Write-Host "It's ok, an email will be sent to you !! `n" -ForegroundColor Green
				}
				else{
					add-content -path $ReportHtml -value "$($dateNow) $($server.Name) not created <br />"
					Write-Host "Problem, an email will be sent to you !! `n" -ForegroundColor Red
				}				
			}
			else {
				Write-Host "$($server.Name) will not be created `n" -ForegroundColor Red
			}
	}
	
Start-VM -VM $($server.Name)


}

# send mail 
	
$MailToBoss = "aali6@myges"
$MailToAsk = "aali6@myges.fr"
$MailToCreator = "tech.ali.ad@gmail.com"
$MailCC = "adnane.ali.sio@gmail.com"
$MailFrom = "ali.adnane5@gmail.com"
$SMTPServer = "smtp.gmail.com"
$SMTPPort = "465" #ssl port
$Subject = "Virtual machine creation report"

Send-MailMessage -From $MailFrom -To $MailToBoss -CC $MailCC -Subject $Subject -Body "Dear director, You will be joining the report on the creation of virtual machines. Here is what was done. Regards," -SmtpServer $SmtpServer -UseSsl -Port $SMTPPort -Credential (Get-Credential) -Attachment $ReportHtml
if($? -eq "true"){
	add-content -path $ReportHtml -value "$($dateNow) Mail send from $($MailFrom) to $($MailToBoss) <br />"
}
else{
	add-content -path $ReportHtml -value "$($dateNow) Mail not send from $($MailFrom) to $($MailToBoss) <br />"
}
	
	
Send-MailMessage -From $MailFrom -To $MailToAsk -CC $MailCC -Subject $Subject -Body "Virtual Machines was created" -SmtpServer $SMTPServer -UseSsl -port $SMTPPort 
if($? -eq "true"){
	add-content -path $ReportHtml -value "$($dateNow) Mail send from $($MailFrom) to $($MailToAsk) <br />"
}
else{
	add-content -path $ReportHtml -value "$($dateNow) Mail not send from $($MailFrom) to $($MailToAsk) <br />"
}
	
	
Send-MailMessage -From $MailFrom -To $MailToCreator -CC $MailCC -Subject $Subject -Body "VM was created and they are OK. you can configure them." -SmtpServer $SMTPServer -UseSsl -port $SMTPPort 
if($? -eq "true"){
	add-content -path $ReportHtml -value "$($dateNow) Mail send from $($MailFrom) to $($MailToCreator) <br />"
}
else{
	add-content -path $ReportHtml -value "$($dateNow) Mail not send from $($MailFrom) to $($MailToCreator) <br />"
}